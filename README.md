# RPG task

A RPG game for you to play alone or with friends.

## Getting Started

The game is under development, so you should rather not get started with it any time soon. Per now you can play around with the heroes in the main program.

## Functionalities

In this game you can do it all. Slay dragons, slay mice, slay goblins, SLAY EVERYTHING. For the more not slaying type of person, functionalities like farming and so on might be added in a not so distant future (No promises). 


## Authors

* **Simen Røstum** - *Initial work*
