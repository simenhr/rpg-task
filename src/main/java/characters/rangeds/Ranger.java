package main.java.characters.rangeds;
// Imports
import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.basestats.CharacterBaseStatsOffensive;
import main.java.characters.abstractions.CharacterCategory;
import main.java.items.armor.Leather;
import main.java.items.armor.Plate;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.armor.Mail;
import main.java.items.weapons.abstractions.WeaponCategory;
import main.java.items.weapons.melee.Axe;
import main.java.items.weapons.melee.Dagger;
import main.java.items.weapons.melee.Sword;
import main.java.items.weapons.ranged.*;

/*
 Rangers
 Are masters of ranged combat. They use a wide arsenal of weapons to dispatch enemies.
*/
public class Ranger extends Ranged {


    public final ArmorType ARMOR_TYPE = ArmorType.Mail;


    // Base stats defensive
    private double baseHealth = CharacterBaseStatsDefensive.RANGER_BASE_HEALTH;
    private double basePhysReductionPercent = CharacterBaseStatsDefensive.RANGER_BASE_PHYS_RED; // Armor
    private double baseMagicReductionPercent = CharacterBaseStatsDefensive.RANGER_BASE_MAGIC_RES; // Magic armor
    // Base stats offensive
    private double baseAttackPower = CharacterBaseStatsOffensive.RANGER_RANGED_ATTACK_POWER;

    // Constructor
    public Ranger() {
        this.armor = armorFactory.getArmor("Mail", rarityFactory.getRarity("Common"));
        this.weapon = rangedWeaponFactory.getWeapon("Bow", rarityFactory.getRarity("Common"));
        this.currentHealth = getCurrentMaxHealth();
    }

    // Public getters statuses and stats
    public double getCurrentMaxHealth() {
        return  baseHealth
                * ((Mail) armor).getHealthModifier()
                * ((Mail) armor).getRarityModifier();
    }  // Needs alteration after equipment is in

    // Equipment behaviours

    /**
     * Equips armor to the character, modifying stats.
     * @param armor
     */
    public void equipArmor(Mail armor) {
        this.armor = armor;
    }

    /**
     * Equips a weapon to the character, modifying stats.
     * @param weapon
     */
    public void equipWeapon(Object weapon) {
        if(weapon instanceof Bow || weapon instanceof Crossbow || weapon instanceof Gun) {
            this.weapon = weapon;
        }
    }

    // Character behaviours

    /**
     * Damages the enemy
     */
    public double attackWithRangedWeapon() {
        if(weapon instanceof Bow) {
            return baseAttackPower
                    * ((Bow) weapon).getAttackPowerModifier()
                    * ((Bow) weapon).getRarityModifier();
        } else if(weapon instanceof Crossbow){
            return baseAttackPower
                    * ((Crossbow) weapon).getAttackPowerModifier()
                    * ((Crossbow) weapon).getRarityModifier();
        }
        return baseAttackPower
                * ((Gun) weapon).getAttackPowerModifier()
                * ((Gun) weapon).getRarityModifier();
    }

    /**
     * Takes damage from an enemy's attack.
     * @param incomingDamage
     * @Param damageType
     */
    public double takeDamage(double incomingDamage, String damageType) {
        switch (damageType){
            case("Magical_Damage"): {
                return incomingDamage*( 1 - baseMagicReductionPercent
                        * ((Mail) armor).getMagicRedModifier()
                        * ((Mail) armor).getRarityModifier());
            }
            case("Physical_Damage"): {
                return incomingDamage*( 1 - basePhysReductionPercent
                        * ((Mail) armor).getPhysRedModifier()
                        * ((Mail) armor).getRarityModifier());
            }
            default:
                return 0;
        }
    }
}
