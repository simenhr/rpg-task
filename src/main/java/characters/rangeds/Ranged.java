package main.java.characters.rangeds;

import main.java.characters.Hero;
import main.java.characters.abstractions.CharacterCategory;
import main.java.factories.ArmorFactory;
import main.java.factories.RangedWeaponFactory;
import main.java.factories.RarityFactory;
import main.java.factories.MagicWeaponFactory;
import main.java.items.weapons.abstractions.WeaponCategory;

// The Ranged class contains all classes dealing damage with ranged weapons
public abstract class Ranged extends Hero {

    RangedWeaponFactory rangedWeaponFactory = new RangedWeaponFactory();
    ArmorFactory armorFactory = new ArmorFactory();
    RarityFactory rarityFactory = new RarityFactory();

    // Metadata
    public final CharacterCategory CHARACTER_CAT = CharacterCategory.Ranged;
    public final WeaponCategory WEAPON_TYPE = WeaponCategory.Ranged;

    @Override
    public abstract double takeDamage(double incomingDamage, String damageType);

}
