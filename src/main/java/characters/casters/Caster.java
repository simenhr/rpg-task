package main.java.characters.casters;

import main.java.characters.Hero;
import main.java.characters.abstractions.CharacterCategory;
import main.java.factories.ArmorFactory;
import main.java.factories.RarityFactory;
import main.java.factories.SpellFactory;
import main.java.factories.MagicWeaponFactory;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.weapons.abstractions.WeaponCategory;
import main.java.spells.abstractions.DamagingSpell;

// The Caster class contains all the spell casters dealing damage
public abstract class Caster extends Hero {

    //Instanciate factories
    MagicWeaponFactory magicWeaponFactory = new MagicWeaponFactory();
    ArmorFactory armorFactory = new ArmorFactory();
    SpellFactory spellFactory = new SpellFactory();
    RarityFactory rarityFactory = new RarityFactory();

    // Metadata
    public final CharacterCategory CHARACTER_CAT = CharacterCategory.Caster;
    public final WeaponCategory WEAPON_TYPE = WeaponCategory.Magic;
    public final ArmorType ARMOR_TYPE = ArmorType.Cloth;
    protected DamagingSpell damagingSpell;

    // Setters

    public void setDamagingSpell (String damagingSpell) {
        this.damagingSpell = (DamagingSpell) spellFactory.getSpell(damagingSpell);
    }

    // Character behaviours
    @Override
    public abstract double takeDamage(double incomingDamage, String damageType);
}
