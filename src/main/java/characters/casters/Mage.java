package main.java.characters.casters;
// Imports
import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.basestats.CharacterBaseStatsOffensive;
import main.java.items.armor.Cloth;
import main.java.items.weapons.magic.Wand;
import main.java.items.weapons.magic.Staff;

/*
 Class description:
 ------------------
 Mages are masters of arcane magic. Their skills are honed after years of dedicated study.
 They conjure arcane energy to deal large amounts of damage to enemies.
 They are vulnerable to physical attacks but are resistant to magic.
*/
public class Mage extends Caster{

    // Base stats defensive
    private double baseHealth = CharacterBaseStatsDefensive.MAGE_BASE_HEALTH;
    private double basePhysReductionPercent = CharacterBaseStatsDefensive.MAGE_BASE_PHYS_RED; // Armor
    private double baseMagicReductionPercent = CharacterBaseStatsDefensive.MAGE_BASE_MAGIC_RES; // Magic armor
    // Base stats offensive
    private double baseMagicPower = CharacterBaseStatsOffensive.MAGE_MAGIC_POWER;

    // Modifiers
    private double defenciveModifier = 0.0;
    private double offensiveModifier = 0.0;

    // Constructor
    public Mage() {
        // When the character is created it has maximum health (base health)
        equipArmor((Cloth) armorFactory.getArmor("Cloth", rarityFactory.getRarity("Common")));
        equipWeapon(magicWeaponFactory.getWeapon("Wand", rarityFactory.getRarity("Common")));
        this.currentHealth = getCurrentMaxHealth();
        setDamagingSpell("ArcaneMissile");
    }

    public Mage(String damagingSpell) {
        equipArmor((Cloth) armorFactory.getArmor("Cloth", rarityFactory.getRarity("Common")));
        equipWeapon(magicWeaponFactory.getWeapon("Wand", rarityFactory.getRarity("Common")));
        this.currentHealth = getCurrentMaxHealth();
        setDamagingSpell("ArcaneMissile");
    }

    // Public getters statuses and stats
    public double getCurrentMaxHealth() {
        return baseHealth
                * ((Cloth) armor).getHealthModifier()
                * ((Cloth) armor).getRarityModifier();
    }


    // Equipment behaviours

    /**
     * Equips armor to the character, modifying stats.
     * @param armor
     */
    public void equipArmor(Cloth armor) {
        this.armor = armor;
    }

    /**
     * Equips a weapon to the character, modifying stats.
     * @param weapon
     */
    public void equipWeapon(Object weapon) {
        if(weapon instanceof Wand || weapon instanceof Staff) {
            this.weapon = weapon;
        }
    }

    // Character behaviours

    /**
     * Damages the enemy with its spells
     */
    public double castDamagingSpell() {
        if(weapon instanceof Wand) {
            return damagingSpell.getSpellDamageModifier()
                    * baseMagicPower
                    * ((Wand) weapon).getMagicPowerModifier()
                    * ((Wand) weapon).getRarityModifier();
        }
        return damagingSpell.getSpellDamageModifier()
                * baseMagicPower
                * ((Staff) weapon).getMagicPowerModifier()
                * ((Staff) weapon).getRarityModifier();
    }

    /**
     * Takes damage from an enemy's attack.
     * @param incomingDamage
     * @Param damageType
     */
    public double takeDamage(double incomingDamage, String damageType) {
        switch (damageType){
            case("Magical_Damage"): {
                return incomingDamage*( 1 - baseMagicReductionPercent
                        * ((Cloth) armor).getMagicRedModifier()
                        * ((Cloth) armor).getRarityModifier());
            }
            case("Physical_Damage"): {
                return incomingDamage*( 1 - basePhysReductionPercent
                        * ((Cloth) armor).getPhysRedModifier()
                        * ((Cloth) armor).getRarityModifier());
            }
            default:
                return 0;
        }
    }
}
