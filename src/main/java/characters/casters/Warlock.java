package main.java.characters.casters;
// Imports
import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.basestats.CharacterBaseStatsOffensive;
import main.java.items.armor.Cloth;
import main.java.items.weapons.magic.Staff;
import main.java.items.weapons.magic.Wand;

/*
 Warlocks are masters of chaos, entropy, and death. They were once mages but were corrupted by power.
 They can conjure up pure chaos energy to destroy their enemies.
*/
public class Warlock extends Caster {



    // Base stats defensive
    private double baseHealth = CharacterBaseStatsDefensive.WARLOCK_BASE_HEALTH;
    private double basePhysReductionPercent = CharacterBaseStatsDefensive.WARLOCK_BASE_PHYS_RED; // Armor
    private double baseMagicReductionPercent = CharacterBaseStatsDefensive.WARLOCK_BASE_MAGIC_RES; // Magic armor
    // Base stats offensive
    private double baseMagicPower = CharacterBaseStatsOffensive.WARLOCK_MAGIC_POWER;

    // Constructor
    public Warlock() {
        // When the character is created it has maximum health (base health)
        equipArmor((Cloth) armorFactory.getArmor("Cloth", rarityFactory.getRarity("Common")));
        equipWeapon(magicWeaponFactory.getWeapon("Wand", rarityFactory.getRarity("Common")));
        this.currentHealth = getCurrentMaxHealth();
        setDamagingSpell("ChaosBolt");
    }

    public Warlock(String damagingSpell) {
        this.currentHealth = getCurrentMaxHealth();
        equipArmor((Cloth) armorFactory.getArmor("Cloth", rarityFactory.getRarity("Common")));
        equipWeapon(magicWeaponFactory.getWeapon("Wand", rarityFactory.getRarity("Common")));
        this.currentHealth = getCurrentMaxHealth();
        setDamagingSpell(damagingSpell);
    }

    // Public getters statuses and stats
    public double getCurrentMaxHealth() {
        return baseHealth
                * ((Cloth) armor).getHealthModifier()
                * ((Cloth) armor).getRarityModifier();
    }

    // Equipment behaviours

    /**
     * Equips armor to the character, modifying stats.
     * @param armor
     */
    public void equipArmor(Cloth armor) {
        this.armor = armor;
    }

    /**
     * Equips a weapon to the character, modifying stats.
     * @param weapon
     */
    public void equipWeapon(Object weapon) {
        if(weapon instanceof Wand || weapon instanceof Staff) {
            this.weapon = weapon;
        }
    }

    // Character behaviours

    /**
     * Damages the enemy with its spells
     */
    public double castDamagingSpell() {
        if(weapon instanceof Wand) {
            return damagingSpell.getSpellDamageModifier()
                    * baseMagicPower
                    * ((Wand) weapon).getMagicPowerModifier()
                    * ((Wand) weapon).getRarityModifier();
        }
        return damagingSpell.getSpellDamageModifier()
                * baseMagicPower
                * ((Staff) weapon).getMagicPowerModifier()
                * ((Staff) weapon).getRarityModifier();
    }

    /**
     * Takes damage from an enemy's attack.
     * @param incomingDamage
     * @Param damageType
     */
    public double takeDamage(double incomingDamage, String damageType) {
        switch (damageType){
            case("Magical_Damage"): {
                return incomingDamage*( 1 - baseMagicReductionPercent
                        * ((Cloth) armor).getMagicRedModifier()
                        * ((Cloth) armor).getRarityModifier());
            }
            case("Physical_Damage"): {
                return incomingDamage*( 1 - basePhysReductionPercent
                        * ((Cloth) armor).getPhysRedModifier()
                        * ((Cloth) armor).getRarityModifier());
            }
            default:
                return 0;
        }
    }
}
