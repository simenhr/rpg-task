package main.java.characters;

// A Hero class that contains all attributes and methods that are the some for all heroes
public abstract class Hero {

    // Equipment
    protected Object armor;
    protected Object weapon;

    // Active trackers and flags
    protected double currentHealth;
    protected Boolean isDead = false;

    // Public getters statuses and stats
    public Boolean getDead() {
        return isDead;
    }

    public double getCurrentHealth() {
        return currentHealth;
    }


    // Character behaviours
    public abstract double takeDamage(double incomingDamage, String damageType);
}

