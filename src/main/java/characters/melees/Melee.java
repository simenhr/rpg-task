package main.java.characters.melees;

import main.java.characters.Hero;
import main.java.characters.abstractions.CharacterCategory;
import main.java.factories.ArmorFactory;
import main.java.factories.MeleeWeaponFactory;
import main.java.factories.RarityFactory;

// The melee class contains all the classes dealing melee damage
public class Melee extends Hero {

    //Instanciate factories
    MeleeWeaponFactory meleeWeaponFactory = new MeleeWeaponFactory();
    ArmorFactory armorFactory = new ArmorFactory();
    RarityFactory rarityFactory = new RarityFactory();

    public final CharacterCategory CHARACTER_CAT = CharacterCategory.Melee;

    @Override
    public double takeDamage(double incomingDamage, String damageType) {
        return 0;
    }
}
