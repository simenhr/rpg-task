package main.java.characters.melees;
// Imports
import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.basestats.CharacterBaseStatsOffensive;
import main.java.characters.abstractions.CharacterCategory;
import main.java.items.armor.Cloth;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.armor.Plate;
import main.java.items.weapons.abstractions.WeaponCategory;
import main.java.items.weapons.magic.Staff;
import main.java.items.weapons.magic.Wand;
import main.java.items.weapons.melee.Hammer;
import main.java.items.weapons.melee.Mace;

/*
 Paladins are faithful servants of the light and everything holy.
 They dispatch justice and are filled with vengeance for the wrongs done to society by evil.
 Paladins are very durable and typically wield heavy weapons.
*/
public class Paladin extends Melee{
    // Metadata
    public final ArmorType ARMOR_TYPE = ArmorType.Plate;
    public final WeaponCategory WEAPON_TYPE = WeaponCategory.BluntWeapon;

    // Base stats defensive
    private double baseHealth = CharacterBaseStatsDefensive.PALADIN_BASE_HEALTH;
    private double basePhysReductionPercent = CharacterBaseStatsDefensive.PALADIN_BASE_PHYS_RED; // Armor
    private double baseMagicReductionPercent = CharacterBaseStatsDefensive.PALADIN_BASE_MAGIC_RES; // Magic armor
    // Base stats offensive
    private double baseAttackPower = CharacterBaseStatsOffensive.PALADIN_MELEE_ATTACK_POWER;


    // Constructor
    public Paladin() {
        // When the character is created it has maximum health (base health)
        equipArmor((Plate) armorFactory.getArmor("Plate", rarityFactory.getRarity("Common")));
        equipWeapon(meleeWeaponFactory.getWeapon("Hammer", rarityFactory.getRarity("Common")));
        this.currentHealth = getCurrentMaxHealth();
    }

    // Public getters statuses and stats
    public double getCurrentMaxHealth() {
        return baseHealth
                * ((Plate) armor).getHealthModifier()
                * ((Plate) armor).getRarityModifier();
    }

    // Equipment behaviours

    /**
     * Equips armor to the character, modifying stats.
     * @param armor
     */
    public void equipArmor(Plate armor) {
        this.armor = armor;
    }

    /**
     * Equips a weapon to the character, modifying stats.
     * @param weapon
     */
    public void equipWeapon(Object weapon) {
        if(weapon instanceof Hammer || weapon instanceof Mace) {
            this.weapon = weapon;
        }
    }

    // Character behaviours

    /**
     * Damages the enemy
     */
    public double attackWithBluntWeapon() {
        if(weapon instanceof Hammer) {
            return baseAttackPower
                    * ((Hammer) weapon).getAttackPowerModifier()
                    * ((Hammer) weapon).getRarityModifier();
        }
        return baseAttackPower
                * ((Mace) weapon).getAttackPowerModifier()
                * ((Mace) weapon).getRarityModifier();
    }

    /**
     * Takes damage from an enemy's attack.
     * @param incomingDamage
     * @Param damageType
     */
    public double takeDamage(double incomingDamage, String damageType) {
        switch (damageType){
            case("Magical_Damage"): {
                return incomingDamage*( 1 - baseMagicReductionPercent
                        * ((Plate) armor).getMagicRedModifier()
                        * ((Plate) armor).getRarityModifier());
            }
            case("Physical_Damage"): {
                return incomingDamage*( 1 - basePhysReductionPercent
                        * ((Plate) armor).getPhysRedModifier()
                        * ((Plate) armor).getRarityModifier());
            }
            default:
                return 0;
        }
    }
}
