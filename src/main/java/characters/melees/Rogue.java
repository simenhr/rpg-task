package main.java.characters.melees;
// Imports
import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.basestats.CharacterBaseStatsOffensive;
import main.java.characters.abstractions.CharacterCategory;
import main.java.items.armor.Cloth;
import main.java.items.armor.Plate;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.armor.Leather;
import main.java.items.weapons.abstractions.WeaponCategory;
import main.java.items.weapons.melee.*;
import main.java.spells.abstractions.DamagingSpell;

/*
 Rogues are stealthy combatants of the shadows.
 They wield bladed weapons with great agility and dispatch their enemies swiftly.
*/
public class Rogue extends Melee {
    // Metadata
    public final ArmorType ARMOR_TYPE = ArmorType.Leather;
    public final WeaponCategory WEAPON_TYPE = WeaponCategory.BladeWeapon;

    // Base stats defensive
    private double baseHealth = CharacterBaseStatsDefensive.ROGUE_BASE_HEALTH;
    private double basePhysReductionPercent = CharacterBaseStatsDefensive.ROGUE_BASE_PHYS_RED; // Armor
    private double baseMagicReductionPercent = CharacterBaseStatsDefensive.ROGUE_BASE_MAGIC_RES; // Magic armor
    // Base stats offensive
    private double baseAttackPower = CharacterBaseStatsOffensive.ROGUE_MELEE_ATTACK_POWER;

    // Constructor
    public Rogue() {
        // When the character is created it has maximum health (base health)
        equipArmor((Leather) armorFactory.getArmor("Leather", rarityFactory.getRarity("Common")));
        equipWeapon(meleeWeaponFactory.getWeapon("Dagger", rarityFactory.getRarity("Common")));
        this.currentHealth = getCurrentMaxHealth();
    }

    // Public getters statuses and stats
    public double getCurrentMaxHealth() {
        return baseHealth
                * ((Leather) armor).getHealthModifier()
                * ((Leather) armor).getRarityModifier();
    }

    // Equipment behaviours

    /**
     * Equips armor to the character, modifying stats.
     * @param armor
     */
    public void equipArmor(Leather armor) {
        this.armor = armor;
    }

    /**
     * Equips a weapon to the character, modifying stats.
     * @param weapon
     */
    public void equipWeapon(Object weapon) {
        if(weapon instanceof Axe || weapon instanceof Dagger || weapon instanceof Sword) {
            this.weapon = weapon;
        }
    }

    // Character behaviours

    /**
     * Damages the enemy
     */
    public double attackWithBladedWeapon() {
        if(weapon instanceof Axe) {
            return baseAttackPower
                    * ((Axe) weapon).getAttackPowerModifier()
                    * ((Axe) weapon).getRarityModifier();
        } else if(weapon instanceof Dagger){
            return baseAttackPower
                    * ((Dagger) weapon).getAttackPowerModifier()
                    * ((Dagger) weapon).getRarityModifier();
        }
        return baseAttackPower
                * ((Sword) weapon).getAttackPowerModifier()
                * ((Sword) weapon).getRarityModifier();
    }

    /**
     * Takes damage from an enemy's attack.
     * @param incomingDamage
     * @Param damageType
     */
    public double takeDamage(double incomingDamage, String damageType) {
        switch (damageType){
            case("Magical_Damage"): {
                return incomingDamage*( 1 - baseMagicReductionPercent
                        * ((Leather) armor).getMagicRedModifier()
                        * ((Leather) armor).getRarityModifier());
            }
            case("Physical_Damage"): {
                return incomingDamage*( 1 - basePhysReductionPercent
                        * ((Leather) armor).getPhysRedModifier()
                        * ((Leather) armor).getRarityModifier());
            }
            default:
                return 0;
        }
    }

}
