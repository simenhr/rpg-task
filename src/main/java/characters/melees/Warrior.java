package main.java.characters.melees;
// Imports
import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.basestats.CharacterBaseStatsOffensive;
import main.java.characters.abstractions.CharacterCategory;
import main.java.items.armor.Leather;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.armor.Plate;
import main.java.items.weapons.abstractions.WeaponCategory;
import main.java.items.weapons.melee.Axe;
import main.java.items.weapons.melee.Dagger;
import main.java.items.weapons.melee.Sword;

/*
 Warriors are combat veterans, durable forces on the battlefield.
 They are masters of the blade and wield it with unmatched ferocity.
*/
public class Warrior extends Melee {
    // Metadata
    public final ArmorType ARMOR_TYPE = ArmorType.Plate;
    public final WeaponCategory WEAPON_TYPE = WeaponCategory.BladeWeapon;

    // Base stats defensive
    private double baseHealth = CharacterBaseStatsDefensive.WARRIOR_BASE_HEALTH;
    private double basePhysReductionPercent = CharacterBaseStatsDefensive.WARRIOR_BASE_PHYS_RED; // Armor
    private double baseMagicReductionPercent = CharacterBaseStatsDefensive.WARRIOR_BASE_MAGIC_RES; // Magic armor
    // Base stats offensive
    private double baseAttackPower = CharacterBaseStatsOffensive.WARRIOR_MELEE_ATTACK_POWER;


    // Constructor
    public Warrior() {
        // When the character is created it has maximum health (base health)
        equipArmor((Plate) armorFactory.getArmor("Plate", rarityFactory.getRarity("Common")));
        equipWeapon(meleeWeaponFactory.getWeapon("Sword", rarityFactory.getRarity("Common")));
        this.currentHealth = getCurrentMaxHealth();
    }

    // Public getters statuses and stats
    public double getCurrentMaxHealth() {
        return baseHealth
                * ((Plate) armor).getHealthModifier()
                * ((Plate) armor).getRarityModifier();
    }

    // Equipment behaviours

    /**
     * Equips armor to the character, modifying stats.
     * @param armor
     */
    public void equipArmor(Plate armor) {
        this.armor = armor;
    }

    /**
     * Equips a weapon to the character, modifying stats.
     * @param weapon
     */
    public void equipWeapon(Object weapon) {
        if(weapon instanceof Axe || weapon instanceof Dagger || weapon instanceof Sword) {
            this.weapon = weapon;
        }
    }

    // Character behaviours

    /**
     * Damages the enemy
     */
    public double attackWithBladedWeapon() {
        if(weapon instanceof Axe) {
            return baseAttackPower
                    * ((Axe) weapon).getAttackPowerModifier()
                    * ((Axe) weapon).getRarityModifier();
        } else if(weapon instanceof Dagger){
            return baseAttackPower
                    * ((Dagger) weapon).getAttackPowerModifier()
                    * ((Dagger) weapon).getRarityModifier();
        }
        return baseAttackPower
                * ((Sword) weapon).getAttackPowerModifier()
                * ((Sword) weapon).getRarityModifier();
    }

    /**
     * Takes damage from an enemy's attack.
     * @param incomingDamage
     * @Param damageType
     */
    public double takeDamage(double incomingDamage, String damageType) {
        switch (damageType){
            case("Magical_Damage"): {
                return incomingDamage*( 1 - baseMagicReductionPercent
                        * ((Plate) armor).getMagicRedModifier()
                        * ((Plate) armor).getRarityModifier());
            }
            case("Physical_Damage"): {
                return incomingDamage*( 1 - basePhysReductionPercent
                        * ((Plate) armor).getPhysRedModifier()
                        * ((Plate) armor).getRarityModifier());
            }
            default:
                return 0;
        }
    }
}
