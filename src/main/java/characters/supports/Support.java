package main.java.characters.supports;

import main.java.characters.Hero;
import main.java.characters.abstractions.CharacterCategory;
import main.java.factories.*;
import main.java.items.weapons.abstractions.WeaponCategory;

// The support classes are the real MVPs. They make sure all the team members stay in the fight by
// helping one way or another
public abstract class Support extends Hero {

    //Instanciate factories
    MagicWeaponFactory magicWeaponFactory = new MagicWeaponFactory();
    ArmorFactory armorFactory = new ArmorFactory();
    RarityFactory rarityFactory = new RarityFactory();
    SpellFactory spellFactory = new SpellFactory();

    public final CharacterCategory CHARACTER_CAT = CharacterCategory.Support;
    public final WeaponCategory WEAPON_TYPE = WeaponCategory.Magic;

    @Override
    public abstract double takeDamage(double incomingDamage, String damageType);
}
