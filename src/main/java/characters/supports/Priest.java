package main.java.characters.supports;
// Imports
import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.characters.abstractions.CharacterCategory;
import main.java.items.armor.Leather;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.armor.Cloth;
import main.java.items.weapons.abstractions.WeaponCategory;
import main.java.items.weapons.magic.Staff;
import main.java.items.weapons.magic.Wand;
import main.java.spells.abstractions.ShieldingSpell;

/*
 Priest are the servants of the light and goodness.
 They use holy magic to heal and shield allies.
 As a support class they only have defensive stats.
*/
public class Priest extends Support{
    // Metadata
    public final ArmorType ARMOR_TYPE = ArmorType.Cloth;
    private ShieldingSpell shieldingSpell;

    // Base stats defensive
    private double baseHealth = CharacterBaseStatsDefensive.PRIEST_BASE_HEALTH;
    private double basePhysReductionPercent = CharacterBaseStatsDefensive.PRIEST_BASE_PHYS_RED; // Armor
    private double baseMagicReductionPercent = CharacterBaseStatsDefensive.PRIEST_BASE_MAGIC_RES; // Magic armor

    // Constructor
    public Priest() {
        this.armor = armorFactory.getArmor("Cloth", rarityFactory.getRarity("Common"));
        this.weapon = magicWeaponFactory.getWeapon("Staff", rarityFactory.getRarity("Common"));
        this.currentHealth = getCurrentMaxHealth();
        this.shieldingSpell = (ShieldingSpell) spellFactory.getSpell("Barrier");
    }

    public Priest(ShieldingSpell shieldingSpell) {
        this.armor = armorFactory.getArmor("Cloth", rarityFactory.getRarity("Common"));
        this.weapon = magicWeaponFactory.getWeapon("Staff", rarityFactory.getRarity("Common"));
        this.currentHealth = getCurrentMaxHealth();
        this.shieldingSpell = shieldingSpell;
    }

    // Public getters statuses and stats
    public double getCurrentMaxHealth() {
        return  baseHealth
                * ((Cloth) armor).getHealthModifier()
                * ((Cloth) armor).getRarityModifier();
    }  // Needs alteration after equipment is in

    // Equipment behaviours

    /**
     * Equips armor to the character, modifying stats.
     * @param armor
     */
    public void equipArmor(Cloth armor) {
        this.armor = armor;
    }

    /**
     * Equips a weapon to the character, modifying stats.
     * @param weapon
     */
    public void equipWeapon(Object weapon) {
        if( weapon instanceof Staff || weapon instanceof Wand) {
            this.weapon = weapon;
        }
    }

    // Character behaviours

    /**
     * Heals the party member, increasing their current health.
     * @param partyMember
     */
    public void healPartyMember(Object partyMember) {

    }

    /**
     * Shields a party member for a percentage of their maximum health.
     * @param partyMemberMaxHealth
     */
    public double shieldPartyMember(double partyMemberMaxHealth) {
        return partyMemberMaxHealth
                * shieldingSpell.getAbsorbShieldPercentage();
    }

    /**
     * Takes damage from an enemy's attack.
     * @param incomingDamage
     * @Param damageType
     */
    public double takeDamage(double incomingDamage, String damageType) {
        switch (damageType){
            case("Magical_Damage"): {
                return incomingDamage*( 1 - baseMagicReductionPercent
                        * ((Cloth) armor).getMagicRedModifier()
                        * ((Cloth) armor).getRarityModifier());
            }
            case("Physical_Damage"): {
                return incomingDamage*( 1 - basePhysReductionPercent
                        * ((Cloth) armor).getPhysRedModifier()
                        * ((Cloth) armor).getRarityModifier());
            }
            default:
                return 0;
        }
    }
}
