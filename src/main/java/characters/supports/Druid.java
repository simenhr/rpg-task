package main.java.characters.supports;
// Imports
import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.characters.Hero;
import main.java.characters.abstractions.CharacterCategory;
import main.java.items.armor.Mail;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.armor.Leather;
import main.java.items.weapons.abstractions.WeaponCategory;
import main.java.items.weapons.ranged.Bow;
import main.java.items.weapons.ranged.Crossbow;
import main.java.items.weapons.ranged.Gun;
import main.java.spells.abstractions.HealingSpell;
import main.java.items.weapons.magic.Staff;
import main.java.items.weapons.magic.Wand;

/*
 Class description:
 ------------------
 Druids are spell casters who use nature based magic to aid their allies in battle.
 They can heal their allies or protect them using the forces of nature.
 As a support class they only have defensive stats.
*/
public class Druid extends Support {
    // Metadata
    public final ArmorType ARMOR_TYPE = ArmorType.Leather;
    private HealingSpell healingSpell;

    // Base stats defensive
    private double baseHealth = CharacterBaseStatsDefensive.DRUID_BASE_HEALTH;
    private double basePhysReductionPercent = CharacterBaseStatsDefensive.DRUID_BASE_PHYS_RED; // Armor
    private double baseMagicReductionPercent = CharacterBaseStatsDefensive.DRUID_BASE_MAGIC_RES; // Magic armor




    // Constructor
    public Druid() {
        this.armor = armorFactory.getArmor("Leather", rarityFactory.getRarity("Common"));
        this.weapon = magicWeaponFactory.getWeapon("Staff", rarityFactory.getRarity("Common"));
        this.currentHealth = getCurrentMaxHealth();
        this.healingSpell = (HealingSpell) spellFactory.getSpell("Regrowth");
    }

    public Druid(HealingSpell healingSpell) {
        this.armor = armorFactory.getArmor("Leather", rarityFactory.getRarity("Common"));
        this.weapon = magicWeaponFactory.getWeapon("Staff", rarityFactory.getRarity("Common"));
        this.currentHealth = getCurrentMaxHealth();
        this.healingSpell = healingSpell;
    }

    // Public getters statuses and stats
    public double getCurrentMaxHealth() {
        return  baseHealth
                * ((Leather) armor).getHealthModifier()
                * ((Leather) armor).getRarityModifier();
    }

    // Equipment behaviours

    /**
     * Equips armor to the character, modifying stats.
     * @param armor
     */
    public void equipArmor(Leather armor) {
        this.armor = armor;
    }

    /**
     * Equips a weapon to the character, modifying stats
     * @param weapon
     */
    public void equipWeapon(Object weapon) {
        if(weapon instanceof Wand || weapon instanceof Staff) {
            this.weapon = weapon;
        }
    }

    // Character behaviours

    /**
     * Heals the party member
     */
    public double healPartyMember() {
        if(weapon instanceof Staff) {
            return healingSpell.getHealingAmount()
                    * ((Staff) weapon).getMagicPowerModifier()
                    * ((Staff) weapon).getRarityModifier();
        }
            return healingSpell.getHealingAmount()
                    * ((Wand) weapon).getMagicPowerModifier()
                    * ((Wand) weapon).getRarityModifier();


    }

    /**
     * Takes damage from an enemy's attack.
     * @param incomingDamage
     * @Param damageType
     */

    public double takeDamage(double incomingDamage, String damageType) {
        switch (damageType){
            case("Magical_Damage"): {
                return incomingDamage*( 1 - baseMagicReductionPercent
                        * ((Leather) armor).getMagicRedModifier()
                        * ((Leather) armor).getRarityModifier());
            }
            case("Physical_Damage"): {
                return incomingDamage*( 1 - basePhysReductionPercent
                        * ((Leather) armor).getPhysRedModifier()
                        * ((Leather) armor).getRarityModifier());
            }
            default:
                return 0;
        }
    }
}
