package main.java;

import main.java.characters.Hero;
import main.java.characters.casters.Mage;
import main.java.characters.melees.Paladin;
import main.java.characters.melees.Warrior;
import main.java.characters.rangeds.Ranger;
import main.java.characters.supports.Priest;
import main.java.factories.*;
import main.java.items.armor.Cloth;
import main.java.items.armor.Leather;
import main.java.items.armor.Mail;
import main.java.items.armor.Plate;
import main.java.items.rarity.*;

import java.util.ArrayList;



public class Main {

    public static void main(String[] args) {
        //Rarities
        Common common = new Common();
        Uncommon uncommon = new Uncommon();
        Rare rare = new Rare();
        Epic epic = new Epic();
        Legendary legendary = new Legendary();
        //----------//
        ArmorFactory armorFactory = new ArmorFactory();
        SpellFactory spellFactory = new SpellFactory();
        MagicWeaponFactory magicWeaponFactory = new MagicWeaponFactory();
        MeleeWeaponFactory meleeWeaponFactory = new MeleeWeaponFactory();
        RangedWeaponFactory rangedWeaponFactory = new RangedWeaponFactory();
        HeroFactory heroFactory = new HeroFactory();

        // Demonstration of creating and adding characters to a party
        // Characters starts with Common rarity items (see each hero class)
        ArrayList<Hero> heroes = new ArrayList<>();
        Paladin achilles = (Paladin) heroFactory.getCharacter("Paladin");
        Mage jaina = (Mage) heroFactory.getCharacter("Mage");
        Priest anduin = (Priest) heroFactory.getCharacter("Priest");
        Ranger robin = (Ranger) heroFactory.getCharacter("Ranger");
        heroes.add(achilles);
        heroes.add(jaina);
        heroes.add(anduin);
        heroes.add(robin);

        // Characters starts with Common rarity items

        // Attack for each of the characters in party
        System.out.println("Attack from each party member");
        System.out.println(achilles.attackWithBluntWeapon());
        System.out.println(jaina.castDamagingSpell());
        System.out.println(anduin.shieldPartyMember(achilles.getCurrentMaxHealth()));
        System.out.println(robin.attackWithRangedWeapon());
        System.out.println("");
        // Taking damage for each character in party
        System.out.println("Damage taken for each party member");
        System.out.println(achilles.takeDamage(100, "Physical_Damage"));
        System.out.println(achilles.takeDamage(100, "Magical_Damage"));
        System.out.println(jaina.takeDamage(100, "Physical_Damage"));
        System.out.println(jaina.takeDamage(100, "Magical_Damage"));
        System.out.println(anduin.takeDamage(100, "Physical_Damage"));
        System.out.println(anduin.takeDamage(100, "Magical_Damage"));
        System.out.println(robin.takeDamage(100, "Physical_Damage"));
        System.out.println(robin.takeDamage(100, "Magical_Damage"));

        // Equiping Rare Items for each hero
        achilles.equipWeapon(meleeWeaponFactory.getWeapon("Hammer", rare));
        achilles.equipArmor((Plate) armorFactory.getArmor("Plate", rare));
        jaina.equipWeapon(magicWeaponFactory.getWeapon("Staff", rare));
        jaina.equipArmor((Cloth) armorFactory.getArmor("Cloth", rare));
        anduin.equipWeapon(meleeWeaponFactory.getWeapon("Staff", rare));
        anduin.equipArmor((Cloth) armorFactory.getArmor("Cloth", rare));
        robin.equipWeapon(rangedWeaponFactory.getWeapon("Bow", rare));
        robin.equipArmor((Mail) armorFactory.getArmor("Mail", rare));
        System.out.println("");

        // Attack for each of the characters in party after equiping epic weapons
        System.out.println("Attack from each party member");
        System.out.println(achilles.attackWithBluntWeapon());
        System.out.println(jaina.castDamagingSpell());
        System.out.println(anduin.shieldPartyMember(achilles.getCurrentMaxHealth()));
        System.out.println(robin.attackWithRangedWeapon());
        System.out.println("");

        // Taking damage for each character in party after equiping epic armor
        System.out.println("Damage taken for each party member");
        System.out.println(achilles.takeDamage(100, "Physical_Damage"));
        System.out.println(achilles.takeDamage(100, "Magical_Damage"));
        System.out.println(jaina.takeDamage(100, "Physical_Damage"));
        System.out.println(jaina.takeDamage(100, "Magical_Damage"));
        System.out.println(anduin.takeDamage(100, "Physical_Damage"));
        System.out.println(anduin.takeDamage(100, "Magical_Damage"));
        System.out.println(robin.takeDamage(100, "Physical_Damage"));
        System.out.println(robin.takeDamage(100, "Magical_Damage"));
    }
}
