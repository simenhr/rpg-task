package main.java.factories;
// Imports
import main.java.items.armor.Cloth;
import main.java.items.armor.Leather;
import main.java.items.armor.Mail;
import main.java.items.armor.Plate;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.rarity.Rarity;

/*
 This factory exists to be responsible for creating new Armor.
 Object is replaced with some Item abstraction.
*/
public class ArmorFactory {
    public Object getArmor(String armorType, Rarity itemRarity){
        return switch (armorType) {
            case "Cloth" -> new Cloth(itemRarity);
            case "Leather" -> new Leather(itemRarity);
            case "Mail" -> new Mail(itemRarity);
            case "Plate" -> new Plate(itemRarity);
            default -> null;
        };
    }
}
