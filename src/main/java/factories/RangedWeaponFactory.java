package main.java.factories;

import main.java.items.rarity.Rarity;
import main.java.items.weapons.magic.Staff;
import main.java.items.weapons.magic.Wand;
import main.java.items.weapons.melee.*;
import main.java.items.weapons.ranged.Bow;
import main.java.items.weapons.ranged.Crossbow;
import main.java.items.weapons.ranged.Gun;

/*
 This factory exists to be responsible for creating new ranged weapons.
 Object is replaced with Weapon as a return type when refactored to be good OO design.
*/
public class RangedWeaponFactory {
    public Object getWeapon(String weaponType, Rarity itemRarity) {
        return switch (weaponType) {
            case "Bow" -> new Bow(itemRarity);
            case "Crossbow" -> new Crossbow(itemRarity);
            case "Gun" -> new Gun(itemRarity);
            default -> null;
        };
    }
}
