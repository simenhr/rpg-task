package main.java.factories;
// Imports
import main.java.characters.casters.Mage;
import main.java.characters.casters.Warlock;
import main.java.characters.melees.Paladin;
import main.java.characters.melees.Rogue;
import main.java.characters.melees.Warrior;
import main.java.characters.rangeds.Ranger;
import main.java.characters.supports.Druid;
import main.java.characters.supports.Priest;

/*
 This factory exists to be responsible for creating new heroes.
 Object is replaced with Character as a return type when refactored to be good OO design.
*/
// TODO Once characters can be made with the right compositions, then implement the factory
public class HeroFactory {
    public Object getCharacter(String heroClass) {
        switch(heroClass) {
            case "Mage":
                return new Mage();
            case "Warlock":
                return new Warlock();
            case "Paladin":
                return new Paladin();
            case "Rouge":
                return new Rogue();
            case "Warrior":
                return new Warrior();
            case "Ranger":
                return new Ranger();
            case "Druid":
                return new Druid();
            case "Priest":
                return new Priest();
            default:
                return null;
        }
    }
}
