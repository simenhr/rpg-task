package main.java.factories;
import main.java.items.rarity.*;


/*
 This factory exists to be responsible for creating new Rarity.
*/
public class RarityFactory {
    public Rarity getRarity(String rarityType) {
        return switch (rarityType) {
            case "Common" -> new Common();
            case "Uncommon" -> new Uncommon();
            case "Rare" -> new Rare();
            case "Epic" -> new Epic();
            case "Legendary" -> new Legendary();
            default -> null;
        };
    }
}
