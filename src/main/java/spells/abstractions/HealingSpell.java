package main.java.spells.abstractions;

public interface HealingSpell extends Spell {
    double getHealingAmount();
}
