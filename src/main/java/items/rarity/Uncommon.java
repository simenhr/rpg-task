package main.java.items.rarity;

import main.java.basestats.ItemRarityModifiers;
import main.java.consolehelpers.Color;

public class Uncommon implements Rarity {
    // Stat modifier
    private double powerModifier = ItemRarityModifiers.UNCOMMON_RARITY_MODIFIER;
    // Color for display purposes
    private String itemRarityColor = Color.GREEN;

    // Public properties
    public double powerModifier() {
        return powerModifier;
    }

    public String getItemRarityColor() {
        return itemRarityColor;
    }
}
