package main.java.items.rarity;

import main.java.consolehelpers.Color;
import main.java.basestats.ItemRarityModifiers;

public class Common implements Rarity{
    // Stat modifier
    private double powerModifier = ItemRarityModifiers.COMMON_RARITY_MODIFIER;
    // Color for display purposes
    private String itemRarityColor = Color.WHITE;

    // Public properties
    public double powerModifier() {
        return powerModifier;
    }

    public String getItemRarityColor() {
        return itemRarityColor;
    }
}
