package main.java.items.rarity;

import main.java.basestats.ItemRarityModifiers;
import main.java.consolehelpers.Color;

public class Legendary implements Rarity {
    // Stat modifier
    private double powerModifier = ItemRarityModifiers.LEGENDARY_RARITY_MODIFIER;
    // Color for display purposes
    private String itemRarityColor = Color.YELLOW;

    // Public properties
    public double powerModifier() {
        return powerModifier;
    }

    public String getItemRarityColor() {
        return itemRarityColor;
    }
}
