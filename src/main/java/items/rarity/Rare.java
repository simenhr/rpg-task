package main.java.items.rarity;

import main.java.basestats.ItemRarityModifiers;
import main.java.consolehelpers.Color;

public class Rare implements Rarity {
    // Stat modifier
    private double powerModifier = ItemRarityModifiers.RARE_RARITY_MODIFIER;
    // Color for display purposes
    private String itemRarityColor = Color.BLUE;

    // Public properties
    public double powerModifier() {
        return powerModifier;
    }

    public String getItemRarityColor() {
        return itemRarityColor;
    }
}
