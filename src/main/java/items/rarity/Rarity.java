package main.java.items.rarity;

public interface Rarity {

    double powerModifier();

    String getItemRarityColor();

}
