package main.java.items.armor;

import main.java.basestats.ArmorStatsModifiers;
import main.java.items.rarity.Rarity;
import main.java.items.rarity.abstractions.ItemRarity;

public class Plate {
    // Stat modifiers
    private double healthModifier = ArmorStatsModifiers.PLATE_HEALTH_MODIFIER;
    private double physRedModifier = ArmorStatsModifiers.PLATE_PHYS_RED_MODIFIER;
    private double magicRedModifier = ArmorStatsModifiers.PLATE_MAGIC_RED_MODIFIER;
    // Rarity
    private final Rarity rarity;

    // Public properties
    public double getHealthModifier() {
        return healthModifier;
    }

    public double getPhysRedModifier() {
        return physRedModifier;
    }

    public double getMagicRedModifier() {
        return magicRedModifier;
    }

    public double getRarityModifier() {
        return rarity.powerModifier();
    }

    // Constructors
    public Plate(Rarity itemRarity) {
        this.rarity = itemRarity;
    }
}
