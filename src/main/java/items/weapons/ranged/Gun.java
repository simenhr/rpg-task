package main.java.items.weapons.ranged;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.rarity.Rarity;
import main.java.items.rarity.abstractions.ItemRarity;

public class Gun {
    // Stat modifiers
    private double attackPowerModifier = WeaponStatsModifiers.GUN_ATTACK_MOD;
    // Rarity
    private Rarity rarity;

    // Public properties
    public double getRarityModifier() {
        return rarity.powerModifier();
    }

    public double getAttackPowerModifier() {
        return attackPowerModifier;
    }

    // Constructors

    public Gun(Rarity itemRarity) {
        this.rarity = itemRarity;
    }
}
