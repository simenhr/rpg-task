package main.java.items.weapons.melee;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.rarity.Rarity;
import main.java.items.rarity.abstractions.ItemRarity;

public class Dagger {
    // Stat modifiers
    private double attackPowerModifier = WeaponStatsModifiers.DAGGER_ATTACK_MOD;
    // Rarity
    private final Rarity rarity;

    // Public properties
    public double getRarityModifier() {
        return rarity.powerModifier();
    }

    public double getAttackPowerModifier() {
        return attackPowerModifier;
    }

    // Constructors

    public Dagger(Rarity itemRarity) {
        this.rarity = itemRarity;
    }
}
