package main.java.items.weapons.melee;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.rarity.Rarity;
import main.java.items.rarity.abstractions.ItemRarity;

public class Mace {
    // Stat modifiers
    private double attackPowerModifier = WeaponStatsModifiers.MACE_ATTACK_MOD;
    // Rarity
    private Rarity rarity;

    // Public properties
    public double getRarityModifier() {
        return rarity.powerModifier();
    }

    public double getAttackPowerModifier() {
        return attackPowerModifier;
    }

    // Constructors

    public Mace(Rarity itemRarity) {
        this.rarity = itemRarity;
    }
}
