package main.java.items.weapons.melee;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.rarity.Rarity;
import main.java.items.rarity.abstractions.ItemRarity;

public class Axe {
    // Stat modifiers
    private double attackPowerModifier = WeaponStatsModifiers.AXE_ATTACK_MOD;
    // Rarity
    private final Rarity rarity;

    // Public properties
    public double getRarityModifier() {
        return rarity.powerModifier();
    }

    public double getAttackPowerModifier() {
        return attackPowerModifier;
    }

    // Constructors
    public Axe(Rarity itemRarity ) {
        this.rarity = itemRarity;
    }
}
