package main.java.items.weapons.melee;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.rarity.Rarity;
import main.java.items.rarity.abstractions.ItemRarity;

public class Hammer {
    // Stat modifiers
    private double attackPowerModifier = WeaponStatsModifiers.HAMMER_ATTACK_MOD;
    // Rarity
    private Rarity rarity;

    // Public properties
    public double getRarityModifier() {
        return rarity.powerModifier();
    }

    public double getAttackPowerModifier() {
        return attackPowerModifier;
    }

    // Constructors

    public Hammer(Rarity itemRarity) {
        this.rarity = itemRarity;
    }
}
