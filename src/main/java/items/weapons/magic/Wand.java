package main.java.items.weapons.magic;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.rarity.Rarity;
import main.java.items.rarity.abstractions.ItemRarity;

public class Wand {
    // Stat modifiers
    private double magicPowerModifier = WeaponStatsModifiers.WAND_MAGIC_MOD;
    // Rarity
    private Rarity rarity;

    // Public properties
    public double getRarityModifier() {
        return rarity.powerModifier();
    }

    public double getMagicPowerModifier() {
        return magicPowerModifier;
    }

    // Constructors

    public Wand(Rarity itemRarity) {
        this.rarity = itemRarity;
    }
}
