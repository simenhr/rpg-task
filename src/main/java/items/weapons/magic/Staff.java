package main.java.items.weapons.magic;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.rarity.Rarity;
import main.java.items.rarity.abstractions.ItemRarity;

public class Staff {

    // Stat modifiers
    private double magicPowerModifier = WeaponStatsModifiers.STAFF_MAGIC_MOD;
    // Rarity
    private Rarity rarity;

    // Public properties
    public double getRarityModifier() {
        return rarity.powerModifier();
    }

    public double getMagicPowerModifier() {
        return magicPowerModifier;
    }

    // Constructors

    public Staff(Rarity itemRarity) {
        this.rarity = itemRarity;
    }
}
